# ssm-eclipse-chapter09

#### 任务
1. 各小组fork 到自己小组项目中
2. 使用自己的数据库，并创建表和数据
3. 每个成员分工调试映射文件，实现 一对一、一对多映射、多对多

#### 数据库sql文件

```

#创建一个名称为tb_idcard的表
CREATE TABLE tb_idcard ( 
id INT PRIMARY KEY AUTO_INCREMENT,
CODE VARCHAR(18)
);

#插入两条数据

INSERT INTO tb_idcard(CODE) VALUES('152221198711020624');
INSERT INTO tb_idcard(CODE) VALUES('152201199008150317');
#创建一个名称为tb_person的表
CREATE TABLE tb_person (
id INT PRIMARY KEY AUTO_INCREMENT,
name VARCHAR(32),
age INT,
sex VARCHAR(8),
card_id INT UNIQUE,
FOREIGN KEY(card_id) REFERENCES tb_idcard(id)
);

#插入两条数据

INSERT INTO tb_person(name, age, sex,card_id) VALUES('Rose',29,'女',1);
INSERT INTO tb_person(name, age, sex,card_id) VALUES('tom',27,'男',2);

#创建一个名称为tb_user的表

CREATE TABLE tb_user (
id int(32) PRIMARY KEY AUTO_INCREMENT,
username varchar (32),
address varchar (256));

#插入3条数据

INSERT INTO tb_user VALUES ('1','詹姆斯','克利夫兰');
INSERT INTO tb_user VALUES ('2','科比,'洛杉矶');
INSERT INTO tb_user VALUES ('3','保罗','洛杉矶');

#创建一个名称为tb_orders的表

CREATE TABLE tb_orders (
id int(32) PRIMARY KEY AUTO_INCREMENT ,
number varchar (32) NOT NULL,
user_id int(32) NOT NULL,
FOREIGN KEY (user_id) REFERENCES tb_user(id)
);

#插入3条数据

INSERT INTO tb_orders VALUES ('1', 11000011','1');
INSERT INTO tb_orders VALUES ('2', 11000012','2');
INSERT INTO tb_orders VALUES ('3', 11000013','3');

#创建一个名称为tb_product的表

CREATE TABLE tb_product(
id INT(32) PRIMARY KEY AUTO_INCREMENT,
NAME VARCHAR(32),
price DOUBLE
);

#插入3条数据

INSERT INTO tb_product VALUES ('1','Java 基础入门','44.5');
INSERT INTO tb_product VALUES ('2','Java Web程序开发入门','38.5');
INSERT INTO tb_product VALUES ('3','SSM 框架整合实战','50');

#创建一个名称为tb_ ordersitem 的中间表

CREATE TABLE tb_ordersitem(
id INT(32) PRIMARY KEY AUTO_INCREMENT,
orders_id INT (32),
product_id INT (32),
FOREIGN KEY (orders_id) REFERENCES tb_orders(id) ,
FOREIGN KEY (product_id) REFERENCES tb_product(id)
);

#插入3条数据

INSERT INTO tb_ordersitem VALUES ('1','1','1');
INSERT INTO tb_ordersitem VALUES ('2','1','3');
INSERT INTO tb_ordersitem VALUES ('3','3','3');

这里输入代码
```