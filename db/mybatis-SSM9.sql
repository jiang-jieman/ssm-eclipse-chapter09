-- phpMyAdmin SQL Dump
-- version 4.4.15.10
-- https://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 2020-10-25 22:55:55
-- 服务器版本： 5.6.49-log
-- PHP Version: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mybatis`
--

-- --------------------------------------------------------

--
-- 表的结构 `account`
--

CREATE TABLE IF NOT EXISTS `account` (
  `id` int(11) NOT NULL,
  `username` varchar(50) DEFAULT NULL,
  `balance` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `tb_idcard`
--

CREATE TABLE IF NOT EXISTS `tb_idcard` (
  `id` int(11) NOT NULL,
  `CODE` varchar(18) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `tb_idcard`
--

INSERT INTO `tb_idcard` (`id`, `CODE`) VALUES
(1, '152221198711020624'),
(2, '152201199008150317');

-- --------------------------------------------------------

--
-- 表的结构 `tb_orders`
--

CREATE TABLE IF NOT EXISTS `tb_orders` (
  `id` int(32) NOT NULL,
  `number` varchar(32) NOT NULL,
  `user_id` int(32) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `tb_orders`
--

INSERT INTO `tb_orders` (`id`, `number`, `user_id`) VALUES
(1, '11000011', 1),
(2, '11000012', 2),
(3, '11000013', 3);

-- --------------------------------------------------------

--
-- 表的结构 `tb_ordersitem`
--

CREATE TABLE IF NOT EXISTS `tb_ordersitem` (
  `id` int(32) NOT NULL,
  `orders_id` int(32) DEFAULT NULL,
  `product_id` int(32) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `tb_ordersitem`
--

INSERT INTO `tb_ordersitem` (`id`, `orders_id`, `product_id`) VALUES
(1, 1, 1),
(2, 1, 3),
(3, 3, 3);

-- --------------------------------------------------------

--
-- 表的结构 `tb_person`
--

CREATE TABLE IF NOT EXISTS `tb_person` (
  `id` int(11) NOT NULL,
  `name` varchar(32) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `sex` varchar(8) DEFAULT NULL,
  `card_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `tb_person`
--

INSERT INTO `tb_person` (`id`, `name`, `age`, `sex`, `card_id`) VALUES
(1, 'Rose', 29, '女', 1),
(2, 'tom', 27, '男', 2);

-- --------------------------------------------------------

--
-- 表的结构 `tb_product`
--

CREATE TABLE IF NOT EXISTS `tb_product` (
  `id` int(32) NOT NULL,
  `NAME` varchar(32) DEFAULT NULL,
  `price` double DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `tb_product`
--

INSERT INTO `tb_product` (`id`, `NAME`, `price`) VALUES
(1, 'Java 基础入门', 44.5),
(2, 'Java Web程序开发入门', 38.5),
(3, 'SSM 框架整合实战', 50);

-- --------------------------------------------------------

--
-- 表的结构 `tb_user`
--

CREATE TABLE IF NOT EXISTS `tb_user` (
  `id` int(32) NOT NULL,
  `username` varchar(32) DEFAULT NULL,
  `address` varchar(256) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `tb_user`
--

INSERT INTO `tb_user` (`id`, `username`, `address`) VALUES
(1, '詹姆斯', '克利夫兰'),
(2, '科比', '洛杉矶'),
(3, '保罗', '洛杉矶');

-- --------------------------------------------------------

--
-- 表的结构 `t_account`
--

CREATE TABLE IF NOT EXISTS `t_account` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `balance` double NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COMMENT='账户表';

--
-- 转存表中的数据 `t_account`
--

INSERT INTO `t_account` (`id`, `username`, `balance`) VALUES
(2, 'joy', 100),
(3, 'Jack', 1800),
(4, 'Rose', 700),
(5, 'tom', 1000),
(6, 'tom', 1000),
(7, 'tom', 1000),
(8, 'tom', 1000),
(9, 'tom', 1000),
(10, 'tom', 1000),
(11, 'tom', 1000),
(12, 'tom', 1000),
(13, 'tom', 1000),
(14, 'tom', 1000),
(15, 'tom', 1000),
(16, 'tom', 1000);

-- --------------------------------------------------------

--
-- 表的结构 `t_customer`
--

CREATE TABLE IF NOT EXISTS `t_customer` (
  `id` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `jobs` varchar(20) NOT NULL,
  `phone` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COMMENT='mybatis用SSM课程';

--
-- 转存表中的数据 `t_customer`
--

INSERT INTO `t_customer` (`id`, `username`, `jobs`, `phone`) VALUES
(1, 'rose', 'student', '13333533092'),
(2, 'rose', 'student', '13333533092'),
(3, 'rose', 'student', '13311111234'),
(5, 'rose', 'student', '13333533092'),
(6, 'rose', 'student', '13333533092'),
(7, 'rose', 'student', '13333533092'),
(8, 'rose', 'student', '13333533092'),
(9, 'rose', 'student', '13333533092'),
(10, 'rose', 'student', '13333533092'),
(11, 'rose', 'student', '13333533092'),
(12, 'rose', 'student', '13333533092');

-- --------------------------------------------------------

--
-- 表的结构 `t_user`
--

CREATE TABLE IF NOT EXISTS `t_user` (
  `t_id` int(11) NOT NULL,
  `t_name` varchar(50) NOT NULL,
  `t_age` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `t_user`
--

INSERT INTO `t_user` (`t_id`, `t_name`, `t_age`) VALUES
(1, 'Lucy', 25),
(2, 'Lili', 20),
(3, 'Jim', 20);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account`
--
ALTER TABLE `account`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_idcard`
--
ALTER TABLE `tb_idcard`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_orders`
--
ALTER TABLE `tb_orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `tb_ordersitem`
--
ALTER TABLE `tb_ordersitem`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orders_id` (`orders_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `tb_person`
--
ALTER TABLE `tb_person`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `card_id` (`card_id`);

--
-- Indexes for table `tb_product`
--
ALTER TABLE `tb_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_account`
--
ALTER TABLE `t_account`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_customer`
--
ALTER TABLE `t_customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_user`
--
ALTER TABLE `t_user`
  ADD PRIMARY KEY (`t_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `account`
--
ALTER TABLE `account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_idcard`
--
ALTER TABLE `tb_idcard`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tb_orders`
--
ALTER TABLE `tb_orders`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_ordersitem`
--
ALTER TABLE `tb_ordersitem`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_person`
--
ALTER TABLE `tb_person`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tb_product`
--
ALTER TABLE `tb_product`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `t_account`
--
ALTER TABLE `t_account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `t_customer`
--
ALTER TABLE `t_customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `t_user`
--
ALTER TABLE `t_user`
  MODIFY `t_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- 限制导出的表
--

--
-- 限制表 `tb_orders`
--
ALTER TABLE `tb_orders`
  ADD CONSTRAINT `tb_orders_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `tb_user` (`id`);

--
-- 限制表 `tb_ordersitem`
--
ALTER TABLE `tb_ordersitem`
  ADD CONSTRAINT `tb_ordersitem_ibfk_1` FOREIGN KEY (`orders_id`) REFERENCES `tb_orders` (`id`),
  ADD CONSTRAINT `tb_ordersitem_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `tb_product` (`id`);

--
-- 限制表 `tb_person`
--
ALTER TABLE `tb_person`
  ADD CONSTRAINT `tb_person_ibfk_1` FOREIGN KEY (`card_id`) REFERENCES `tb_idcard` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
